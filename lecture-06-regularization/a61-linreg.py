import numpy as np
import matplotlib.pyplot as plt

X = np.loadtxt(open("x-linreg.dat"), delimiter=",")
y = np.loadtxt(open("y-linreg.dat"), delimiter=",")

X = np.vstack((np.ones(X.shape), X, X**2, X**3, X**4, X**5)).T

lambdas = [0., 0.1, 10.]

for l in lambdas:

    # --- INSERT YOUR CODE HERE -------
    #     first to build L and then to compute theta using the closed form equation

    L =
    theta = 

    # ---------------------------------

    xl = np.linspace(-1,1,50)
    xl = np.vstack((np.ones(xl.shape), xl, xl**2, xl**3, xl**4, xl**5)).T
    plt.figure()
    plt.scatter(X[:,1],y)
    plt.plot(xl[:,1], xl.dot(theta))

plt.show()
