import numpy as np
from gd import gradient_descent

def f(x,y):
  return 1+(x**2+y**2)/5

def dfx(x,y):
  return x*2/5

def dfy(x,y):
  return y*2/5


init_point = np.random.random(2)*5
print "init point ", init_point
(x,y, n_iters) = gradient_descent(f,dfx,dfy,init=init_point)
print "min= ",f(x,y), " @(x,y)=(",x,",",y,") iters",n_iters

# repeat the call to gradient_descent no with step_size=5.0 and show_progress=True

