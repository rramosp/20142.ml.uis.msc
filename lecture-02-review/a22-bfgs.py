import numpy as np
from scipy.optimize import minimize

# observa como ahora definimos esta funcion
# de una sola variable que ha de ser un vector 
# con dos componentes
def f(x):
  return 1+(x[0]**2+x[1]**2)/5

def f_with_jacobian(x):
  pass
  # your code here

init_point = np.random.random(2)*100+10000
r = minimize(f, init_point, method="BFGS", options={ "disp": True} )
print "minumum at ", r.x, " f(x_min)",f(r.x)

# call again function minimize with the Jacobian

