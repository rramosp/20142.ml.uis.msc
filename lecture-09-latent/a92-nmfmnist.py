import glob, shutil, os
import numpy as np
from bootstrap import *	
from metrics import *
from matplotlib.pyplot import *
from sklearn.naive_bayes import GaussianNB
from sklearn.decomposition import ProjectedGradientNMF

data = np.loadtxt("mnist4k.csv", delimiter=",")
imgs = data[:,1:]
labels = data[:,0]
print "loaded images ",imgs.shape

# ----- YOUR CODE HERE ----
# use ProjectedGradientNMF with 15 components and random_state=0 to
# find an nmf factorization of the data using the 'fit' method of the
# ProjectedGradientNMF object, and then transform the data with the
# 'transform' method
#
# then use a GaussianNB to classify first the original data and then the
# transform data in the same way as in exercise a92
# --------------------------

# interpret rV as eigenimgs
print "saving component imgs .... ", nmf.components_.shape
dir="/tmp/mnist-nmf-basis"
shutil.rmtree(dir, ignore_errors=True)
os.makedirs(dir)
i=0
for base_img in nmf.components_:
    imshow(base_img.reshape(28,28), aspect="equal", interpolation="nearest", cmap = cm.Greys_r)
    savefig(dir+"/img"+str(i)+".jpg")
    i += 1

