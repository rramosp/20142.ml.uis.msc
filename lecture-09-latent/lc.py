import numpy as np
from bootstrap import *
from metrics import *
import matplotlib.pyplot as plt
from pylab import *

def learning_curve(classifier, data, labels, score_function=compute_ACC, nb_bootstraps=10, step_size=0.1, new_figure=True, legend_label=None, debug=False):
    test_pcts = np.arange(step_size, 1, step_size)
    testScores = np.array([])
    trainScores = np.array([])
    trainStdevs = np.array([])
    testStdevs  = np.array([])
    if debug:
	print "classifier "+str(classifier)
    for pct in test_pcts:
        if debug:
		print "   learning curve at ",pct
        s=bootstrap_score(classifier, data, labels, score_function, nb_bootstraps, pct)
        mean_scores = s.mean(axis=1)
        std_scores  = s.std(axis=1)
        trainScores = np.append(trainScores, mean_scores[0])
        testScores = np.append(testScores, mean_scores[1])
        trainStdevs = np.append(trainStdevs, std_scores[0])
        testStdevs  = np.append(testStdevs, std_scores[1])
    
    if new_figure:
	figure()
    train_pcts = 1 - test_pcts
    plt.errorbar(train_pcts, trainScores, yerr=trainStdevs, c="b", label="train")
    plt.errorbar(train_pcts, testScores, yerr=testStdevs, c="r", label="test")
    ylim([0,1])
    if legend_label==None:
       legend_label = str(classifier)+" "+str(data.shape)
    legend(title=legend_label, loc=8)

