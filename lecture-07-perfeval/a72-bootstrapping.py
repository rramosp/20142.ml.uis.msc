import numpy as np
from metrics import *
from bootstrap import *
from sklearn.tree import DecisionTreeClassifier


data = np.loadtxt("contaminacion-20k-9attrs.csv", delimiter=",", skiprows=1)

# --- YOUR CODE HERE ----
# extract columns 1 to 10 into a single data structure
train_data = ?????

# extract first column and threshold it (class 1 if >6, class 0 otherwise)
train_labels = ???
# --------------------

print "positive instances "+str(np.sum(train_labels==1))
print "negative instances "+str(np.sum(train_labels==0))

c = DecisionTreeClassifier()

nb_bootstraps=10
for i in [0.01, 0.05, 0.1, 0.2,  0.5, 0.8]:
   s = bootstrap_score(c, train_data, train_labels, score_function=compute_ACC, nb_bootstraps=nb_bootstraps, test_size=i)
   print "----- test size = "+str(i)+", bs = "+str(nb_bootstraps)+", mean scores: "+str( s.mean(axis=1))+", std: "+str(s.std(axis=1))
