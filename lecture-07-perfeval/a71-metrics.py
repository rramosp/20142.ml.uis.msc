import numpy as np
from lr import *
from metrics import *
from sklearn.datasets import *

data = np.loadtxt("mnist-small.csv", delimiter=",")

# extract columns 0 to 784 into a single data structure
train_data = data[:,0:784]

# extract column 785 (which is column nb 784 if starting at index 0)
train_labels = data[:,784]

lambda_list = [0.0, 0.2]
i=1
for l in lambda_list:
  print "-------"
  print "lambda = "+str(l)

  lr = LogisticRegression(0.001, lmbd=l)
  lr.fit(train_data, train_labels)

  predicted_labels = lr.predict(train_data)

  print "classifier score (accuracy)="+str(lr.score(train_data, train_labels))

  print "tpr = "+str(compute_TPR(train_labels, predicted_labels))
  print "tnr = "+str(compute_TNR(train_labels, predicted_labels))
  print "fnr = "+str(compute_FNR(train_labels, predicted_labels))
  print "fpr = "+str(compute_FPR(train_labels, predicted_labels))
  print "ppv = "+str(compute_PPV(train_labels, predicted_labels))
  print "npv = "+str(compute_NPV(train_labels, predicted_labels))
  print "acc = "+str(compute_ACC(train_labels, predicted_labels))
  print "f1s = "+str(compute_F1S(train_labels, predicted_labels))
