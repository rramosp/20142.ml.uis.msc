import numpy as np
from lro import *
from lc import *
from plotdata import *
from sklearn.datasets import *

def quad_features(X):
    return np.hstack((X, X**3))

def polynomial_features(X):
    x = np.array([X[:,0]]).T
    y = np.array([X[:,1]]).T
    
    r = x
    for i in range(1,7):
        for j in range(0,i+1):
            r = np.hstack((r,x**(i-j)*y**j))
    return r   

def plot_lcurve_and_boundary(classifier, data, labels, nb_bootstraps=30, legend_label=None):
	figure()
	plt.subplot(1,2,1)
	learning_curve(classifier, data, labels, nb_bootstraps=nb_bootstraps, new_figure=False, legend_label=legend_label)

	plt.subplot(1,2,2)
	classifier.fit(data,labels)
	plot_boundary_with_data(lr, data,labels)

n_samples = 200

(data, labels) = make_moons(n_samples=n_samples, noise=0.15)
lr = LogisticRegressionO(lmbd=0.0, map_features_function=polynomial_features)
plot_lcurve_and_boundary(lr, data, labels, legend_label="l=0.0, pol features ")

lr = LogisticRegressionO(lmbd=0.1, map_features_function=polynomial_features)
plot_lcurve_and_boundary(lr, data, labels, legend_label="l=0.1, pol features")

lr = LogisticRegressionO(lmbd=0.1, map_features_function=quad_features)
plot_lcurve_and_boundary(lr, data, labels, legend_label="l=0.1, quad features")

lr = LogisticRegressionO(lmbd=0.1)
plot_lcurve_and_boundary(lr, data, labels, legend_label="l=0.1, original features")

plt.show()

